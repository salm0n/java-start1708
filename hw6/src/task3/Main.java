package task3;


import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int[] arr = {5, 45, 87, 2, 42, 0 ,2};
        System.out.println(linearSearch(arr, 2));
        System.out.println(modifiedLinearSearch(arr, 2));
    }


    public static int linearSearch(int[] arr, int number){

        for (int i = 0; i < arr.length; i++){
            if (number == arr[i]){
                return i;
            }
        }
        return -1;
    }


    public static int modifiedLinearSearch(int[]arr, int number){

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < arr.length; i++){
            list.add(arr[i]);
        }

        list.add(number);

        int index = 0;
        int lastIndex = list.size() - 1;
        while(list.get(index) != number){
            index++;
        }
        list.remove(lastIndex);
        if(index != lastIndex){
            return index;
        }
        return -1;
    }

}
