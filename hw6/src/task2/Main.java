package task2;

public class Main {
    public static void main(String[] args) {
        drawRectangle(5,10);
    }

    public static void drawRectangle(int height, int width){
        for (int i = 1; i <= height; i++){
            for (int j = 1; j <= width; j++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
