//1)Напишите метод который вернет максимальное число из массива целых чисел

package task1;

public class Main {
    public static void main(String[] args) {
        int[] arr = {1, 2, 14, 3, 8, 4};
        System.out.println(getMaxNumber(arr));
    }

    public static int getMaxNumber(int[] arr){
        int maxNumber = arr[0];
        for (int i = 0; i < arr.length; i++){
            if (maxNumber < arr[i]){
                maxNumber = arr[i];
            }
        }
        return maxNumber;
    }
}
