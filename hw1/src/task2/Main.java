package task2;

//Один литр топлива стоит 1.2$. Ваш автомобиль тратит на 100 км пути 8 литров топлива.
//Вы собрались в поездку в соседний город. Расстояние до этого города составляет 120
//км. Вычислите и выведите на экран сколько вам нужно заплатить за топливо для
//поездки.

public class Main {
    public static void main(String[] args) {
        double fuelCost = 1.2;
        double distance = 120;
        double fuelConsumption = 8;


        double price = fuelConsumption / 100  * distance * fuelCost;
        System.out.println(price + "$");
    }
}
