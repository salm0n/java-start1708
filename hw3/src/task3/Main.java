//Выведите на экран прямоугольник из *. Причем высота и ширина прямоугольника вводятся с
//клавиатуры. Например ниже представлен прямоугольник с высотой 4 и шириной 5.
//        *****
//        *   *
//        *   *
//        *****


package task3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("input height");
        int h = scanner.nextInt();
        System.out.println("input width");
        int w = scanner.nextInt();


        for (int i = 1; i <= h; i++){

            for (int j = 1; j <= w; j++){
                if (i == 1 || i == h){
                    System.out.print("*");
                }else {
                    if (j == 1 || j == w){
                        System.out.print("*");
                    }else{
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
