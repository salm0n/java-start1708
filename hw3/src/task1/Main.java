//Вычислить с помощью цикла факториал числа - n введенного с клавиатуры (4<n<16). Факториал
//числа это произведение всех чисел от этого числа до 1. Например 5!=5*4*3*2*1=120



package task1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userNum = scanner.nextInt();
        long factorial = 1;

        if (4 < userNum && userNum < 16){
            for (int i = 1; i <= userNum; i++){
                factorial = factorial * i;
                System.out.println(factorial);
            }

        }else{
            System.out.println("Не соблюдены условия: (4 < ваше число < 16)");
        }
    }
}
