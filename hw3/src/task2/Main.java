//Напечатайте таблицу умножения на 5. предпочтительно печатать 1 x 5 = 5, 2 x 5 = 10, а не просто 5,10 и т. д.


package task2;

public class Main {
    public static void main(String[] args) {

        int num;


        for (int i = 1; i <= 10; i++){
            num = 5 * i;
            System.out.println(i + " x 5 = " + num);
        }
    }
}
