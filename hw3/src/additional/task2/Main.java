//С помощью вложенных циклов выведите на экран «песочные часы», максимальная ширина
//которых считывается с клавиатуры (число нечетное). В примере ширина равна 5.


package additional.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int userNum = sc.nextInt();

        if (userNum % 2 != 0){

            for (int i = 0; i < userNum; i++){
                for (int j = 0; j < userNum; j++){
                    if (j > i && j > userNum - i - 1 || j < i && j < userNum - i - 1) {
                        System.out.print(" ");
                    }else{
                        System.out.print("*");
                    }
                }
                System.out.println();
            }

        }else{
            System.out.println("Only odd numbers");
        }
    }
}
