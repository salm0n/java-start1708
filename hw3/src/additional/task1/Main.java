//С помощью цикла (только одного) нарисовать фигуру изображенную ниже. Максимальная высота
//этой фигуры вводится с клавиатуры (в примере максимальная высота — 4.).


package additional.task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("input height: ");
        int h = sc.nextInt();
        int count1 = 1;
        int count2 = 1;
        boolean log = false;


        for (int i = 1; i <= Math.pow(h, 2); i++){
            if (count1 <= count2){
                System.out.print("*");
                count1++;
            }else{
                i--;
                System.out.println();
                count1 = 1;
                if (count2 < h && !log){
                    count2++;
                }else{
                    log = true;
                    count2--;
                }
            }
        }
    }
}
