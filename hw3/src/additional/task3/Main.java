//С помощью цикла реализуйте алгоритм нахождения вещественного корня используя
//итерационную формулу Герона.


package additional.task3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input number");
        double number = sc.nextDouble();
        double x0 = number;
        double err = 0.1;
        double x1 = 1 / 2.0 * (x0 + number / x0);
        double d = Math.abs(x1 - x0);

        while(d >= 2 * err && d * d >= 2 * err) {
            x0 = x1;
            x1 = 1/2.0 * (x0 + number/x0);
            d = Math.abs(x1 - x0);
        }

        System.out.println(x1);
        System.out.println(Math.sqrt(number));

    }
}
