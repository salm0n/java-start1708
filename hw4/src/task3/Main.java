//Создать массив случайных чисел (размером 15 элементов). Создайте второй массив в два раза
//больше, первые 15 элементов должны быть равны элементам первого массива, а остальные
//элементы заполнить удвоенных значением начальных. Например:
//Было → {1,4,7,2}
//Стало → {1,4,7,2,2,8,14,4}


package task3;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args){
        Random random = new Random();
        int[] arr = new int[15];
        int[] arr2 = new int[30];

        for (int j = 0; j < arr.length; j++){
            arr[j] = random.nextInt(10);
            arr2[j] = arr[j];
        }
        for (int i = 0; i < arr2.length; i++){
            if(i < 15){
                arr2[i + 15] = arr2[i] * 2;
            }
        }
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
    }
}
