//Написать код для возможности создания массива целых чисел (размер вводиться с клавиатуры) и
//возможности заполнения каждого его элемента вручную. Выведите этот массив на экран.


package task2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("array size");
        int arrSize = sc.nextInt();
        int[] arr = new int[arrSize];

        for (int i = 0; i < arr.length; i++){
            System.out.println("input number");
            arr[i] = sc.nextInt();
        }
        System.out.println(Arrays.toString(arr));
    }
}
