package task2;

import java.util.Scanner;

//Треугольник существует только тогда, когда сумма любых двух его сторон больше третьей. Дано: a,
//b, c – стороны предполагаемого треугольника. Напишите программу, которая укажет, существует ли
//такой треугольник или нет.

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double sideA;
        double sideB;
        double sideC;

        System.out.println("Перша сторона: ");
        sideA = sc.nextInt();
        System.out.println("Друга сторона: ");
        sideB = sc.nextInt();
        System.out.println("Третя сторона: ");
        sideC = sc.nextInt();

        if(sideA + sideB > sideC && sideC + sideB > sideA && sideA + sideC > sideB) {
            System.out.println("Вітаю!!! Це трикутник!!!");
        }else {
            System.out.println("Трикутником тут не пахне");
        }
    }
}
