//Есть круг с центром в начале координат и радиусом 4. Пользователь вводит с клавиатуры
//координаты точки x и y (вещественные числа). Написать программу которая определит лежит ли
//эта точка внутри круга или нет.


package additional.task3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double pointX = scanner.nextInt();
        double pointY = scanner.nextInt();
        int r = 4;

        if (Math.pow(pointX, 2) + Math.pow(pointY, 2) <= Math.pow(r, 2)){
            System.out.println("точка в круге");
        }else{
            System.out.println("Точка вне круга");
        }
    }
}
