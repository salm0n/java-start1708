//Дан треугольник координаты вершин А(0,0), В(4,4), С(6,1). Пользователь вводит с клавиатуры
//координаты точки x и y (вещественные числа). Написать программу которая определит лежит ли
//эта точка внутри треугольника или нет.


package additional.task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double coordXvertexA = 0;
        double coordYvertexA = 0;
        double coordXvertexB = 4;
        double coordYvertexB = 4;
        double coordXvertexC = 6;
        double coordYvertexC = 1;

        double pointX = sc.nextDouble();
        double pointY = sc.nextDouble();

        double a = (coordXvertexA - pointX) * (coordYvertexB - coordYvertexA) - (coordXvertexB - coordXvertexA) * (coordYvertexA - pointY);
        double b = (coordXvertexB - pointX) * (coordYvertexC - coordYvertexB) - (coordXvertexC - coordXvertexB) * (coordYvertexB - pointY);
        double c = (coordXvertexC - pointX) * (coordYvertexA - coordYvertexC) - (coordXvertexA - coordXvertexC) * (coordYvertexC - pointY);


        if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)){
            System.out.println("Точка лежит в треугольнике");
        }else {
            System.out.println("Точка не лежит в треугольнике");
        }
    }
}

