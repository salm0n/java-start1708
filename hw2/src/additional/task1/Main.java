//Пользователь вводит целое четырехзначное число. Проверить, является ли оно номером
//«счастливого билета». Примечание: счастливым билетом называется число, в котором, при четном
//количестве цифр в числе, сумма цифр его левой половины равна сумме цифр его правой
//половины. Например, рассмотрим число 1322. Его левая половина равна 13, а правая 22, и оно
//является счастливым билетом (т. к. 1 + 3 = 2 + 2).


package additional.task1;

import java.util.Scanner;

public class Main {
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите четырехзначное число");
        int userNumber = sc.nextInt();


        if (userNumber / 1000 < 10 && userNumber / 1000 > 0){
            int num1 = userNumber / 1000;
            int num2 = userNumber / 100 % 10;
            int num3 = userNumber / 10 % 10;
            int num4 = userNumber % 10;

            if (num1 + num2 == num3 + num4){
                System.out.println("Поздравляем! У вас счастливый билет!");
            }else{
                System.out.println("Увы, у вас несчастливый билет");
            }
        }else{
            System.out.println("Вы ввели не четырехзначное число");
        }
    }
}
