//С клавиатуры вводится целое шестизначное число. Проверить, является ли оно палиндромом.
//Примечание: палиндромом называется число, слово или текст, которые одинакового читаются
//слева направо и справа налево. Например, это числа 143341, 552255, 710017 и т. д.


package additional.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a 6 digit number");
        int number = sc.nextInt();
        if (number / 100000 < 10 && number / 100000 > 0){

            int reversed = 0;
            int partOfUserNum = number % 1000;

            while(partOfUserNum != 0) {
                int digit = partOfUserNum % 10;
                reversed = reversed * 10 + digit;
                partOfUserNum /= 10;
            }

            if(number / 1000 == reversed){
                System.out.println("its a palindrome!");
            }else{
                System.out.println("its a not palindrome");
            }

        }else{
            System.out.println("you entered not a 6 digit number");
        }
    }
}
