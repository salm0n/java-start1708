package task3;

import java.util.Scanner;

//Есть девятиэтажный дом, в котором 5 подъездов. Номер подъезда начинается с единицы. На
//одном этаже 4 квартиры. Напишите программу которая, получит номер квартиры с клавиатуры, и
//выведет на экран, на каком этаже, какого подъезда расположена эта квартира. Если такой
//квартиры нет в этом доме, то нужно сообщить об этом пользователю.

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your apartment number");
        int apartmNum = sc.nextInt();
        int entrance;
        int floorNum;


        if (apartmNum > 0 && apartmNum <= 180){
            entrance = apartmNum % 36 == 0 ? apartmNum/36 : apartmNum /36 + 1;

            if (apartmNum % 36 == 0){
                floorNum = 9;
            }else if(apartmNum % 4 == 0){
                floorNum = apartmNum % 36 / 4;
            }else{
                floorNum = apartmNum % 36 / 4 + 1;
            }

            System.out.println("entrance: " + entrance);
            System.out.println("floor: " + floorNum);
        }else{
            System.out.println("There is no such apartment");
        }
    }
}