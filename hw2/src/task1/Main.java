package task1;

import java.util.Scanner;

//Написать программу которая считает 4 целых числа с клавиатуры и выведет на экран самое
//большое из них.

public class Main {
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        int number1;
        int number2;
        int number3;
        int number4;
        int maxNum;

        System.out.println("Enter the number1");
        number1 = sc.nextInt();

        System.out.println("Enter the number2");
        number2 = sc.nextInt();

        System.out.println("Enter the number3");
        number3 = sc.nextInt();

        System.out.println("Enter the number4");
        number4 = sc.nextInt();


        maxNum = number1;
        if (number2 > maxNum) {
            maxNum = number2;
        }
        if (number3 > maxNum) {
            maxNum = number3;
        }
        if (number4 > maxNum){
            maxNum = number4;
        }
        System.out.println(maxNum);
    }
}
