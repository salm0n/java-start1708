package sample;

import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("specify the path");
        File sample = new File(sc.nextLine());
        getListOfFolder(sample);
    }

    public static void getListOfFolder (File folder){
        File[] arrFold = folder.listFiles();
        for (int i = 0; i < arrFold.length; i++){
            if (arrFold[i].isDirectory()){
                System.out.println(arrFold[i].getName());
            }
        }
    }
}