package sample;

import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;


public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("file name: ");
        String fileName = sc.nextLine();
        File file = new File(fileName + ".txt");
        writer(file);
    }


    public static void writer(File file){
        Scanner sc = new Scanner(System.in);
        System.out.print("input text: ");
        String userText = sc.nextLine();
        try (PrintWriter print = new PrintWriter(file)){
            print.println(userText);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
