package sample;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int column = sc.nextInt();
        int row = sc.nextInt();
        int bound = sc.nextInt();
        create2DArr(row, column, bound);
    }

    private static void printArrInFile(int[][] arr){
        File file = new File("a4rr.txt");
        try(PrintWriter pr = new PrintWriter(file)){
            for (int i = 0; i < arr.length; i++){
                pr.println(Arrays.toString(arr[i]));
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void create2DArr(int x, int y, int bound){
        Random rd = new Random();
        int[][] arr = new int[x][y];
        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr[i].length; j++){
                arr[i][j] = rd.nextInt(bound);
            }
        }
        printArrInFile(arr);
    }
}
