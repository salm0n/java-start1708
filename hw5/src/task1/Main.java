//1)Считайте строку текста с клавиатуры. Подсчитайте сколько раз в нем встречается символ «b».


package task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the text");
        String userStr = sc.nextLine();
        System.out.println(letterCounter(userStr, 'b'));
    }

    public static int letterCounter(String str, char ltr){
        int count = 0;

        char[] strArr = str.toLowerCase().toCharArray();

        for (int i = 0; i < strArr.length; i++){
            if(strArr[i] == ltr){
                count++;
            }
        }
        return count;
    }
}
