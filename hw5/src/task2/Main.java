//2) Вводится строка из слов, разделенных пробелами. Найти самое длинное слово и вывести его на
//экран.

package task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String userStr = sc.nextLine();
        System.out.println(getLongestWord(userStr));
    }

    public static String getLongestWord(String str){
        String[] word = str.split("[ ]");
        String longestWord = word[0];

        for (int i = 0; i < word.length; i++){
            if(longestWord.length() < word[i].length()){
                longestWord = word[i];
            }
        }

        return longestWord;
    }
}
