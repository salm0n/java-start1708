//Выведите на экран 10 строк со значением числа Пи. Причем в первой строке должно быть 2 знака
//после запятой, во второй 3, в третьей 4 и т. д.


package task3;

public class Main {
    public static void main(String[] args) {
        printPi(10);
    }

    public static void printPi(int item){
        for (int i = 0; i < item; i++){
            String result = String.format("%."+ (i + 2) +"f", Math.PI);
            System.out.print(i + 1 + ".  ");
            System.out.println(result);
        }
    }
}
